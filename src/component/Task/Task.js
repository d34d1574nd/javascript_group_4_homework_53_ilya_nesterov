import React from 'react';
import './Task.css';

const Task = props => {
    return (
        <p className="messageToDo">{props.value}
            <input type="checkbox" id="checked"/><label htmlFor="checked">Выполнено!</label>
            <button className="close" onClick={props.remove}>x</button>
        </p>
)
};


export default Task;