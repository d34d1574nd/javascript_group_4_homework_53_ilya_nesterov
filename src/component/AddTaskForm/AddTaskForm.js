import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = (props) => (
    <div className="addTaskForm">
        <form action="#">
            <input
                type="text"
                className="message"
                placeholder='Enter the message To Do List'
                onChange={props.change}
            />
            <button className='button' onClick={props.submit}>Add work</button>
        </form>
    </div>
);

export default AddTaskForm;