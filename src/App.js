import React, { Component } from 'react';
import AddTaskForm from './component/AddTaskForm/AddTaskForm'
import Task from './component/Task/Task'
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      list: [
        {id: 0, value: 'Купить подарки на Новый Год!'},
        {id: 1, value: 'Купить продукты на Новый Год!'},
        {id: 2, value: 'Купить подарок бабушке на день рождения (2е января)!'}
      ],
      listId: 2
    };
  }

  change = (event) => {
    this.setState({
      value: event.target.value
    });
  };

  submit = () => {
    if (this.state.value === '') {
      return false
    } else {
      this.list = [...this.state.list, {id: this.state.listId + 1, value: this.state.value}];
      this.setState({
        value: '',
        list: this.list
      });
    }
  };

  remove = id => {
    let list = this.state.list;
    let index = list.findIndex(task => task.id === id);
    list.splice(index, 1);

    this.setState({
        list: list,
        listId: this.state.listId - 1
    });
  };

  render() {
      let list = this.state.list.map((task) => {
          return (
              <Task
                  key={task.id}
                  remove={() => this.remove(task.id)}
                  value={task.value}
              />
              )
      });
    return (
      <div className="App">
          <AddTaskForm
              change={event => this.change(event)}
              submit={() => this.submit()}
          />
          {list}
      </div>
    );
  }
}

export default App;
